import 'package:flutter/material.dart';

class NameListItem extends StatelessWidget {
  final String name;

  const NameListItem({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(name),
      )
    );
  }

}
