import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';

class ReadNFCView extends StatefulWidget {
  const ReadNFCView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ReadNFCViewState();

}

class _ReadNFCViewState extends State<ReadNFCView> {

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: startScan,
      child: const Text("Start")
    );
  }

  void startScan() {
    NfcManager.instance.startSession(
      onDiscovered: (NfcTag tag) async {
        final snackBar = SnackBar(
          content: Text(String.fromCharCodes(tag.data["ndef"]["cachedMessage"]["records"][1]["payload"])),
          duration: const Duration(seconds: 10),
          action: SnackBarAction(
            label: "Stop Scan",
            onPressed: stopScan,
          ),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      },
    );
  }

  void stopScan() {
    NfcManager.instance.stopSession();
  }
}
