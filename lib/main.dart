import 'package:flutter/material.dart';
import 'package:hello_world/bottom_nav_bar.dart';
import 'package:hello_world/bottom_nav_bar_item.dart';
import 'package:hello_world/name_list_view.dart';
import 'package:hello_world/read_nfc_view.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BottomNavBar(
        items: [
          BottomNavBarItem(
            "Name List App", const NameListView(), "Name List", const Icon(Icons.home)
          ),
          BottomNavBarItem(
              "Read NFC", const ReadNFCView(), "NFC", const Icon(Icons.home)
          )
        ],
      ),
    );
  }
}
