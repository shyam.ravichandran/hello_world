import 'package:flutter/cupertino.dart';

class BottomNavBarItem {
  late String title;
  late Widget content;
  late String label;
  late Icon icon;

  BottomNavBarItem(this.title, this.content, this.label, this.icon);
}