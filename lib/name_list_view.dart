import 'package:flutter/material.dart';
import 'package:hello_world/name_list_item.dart';

class NameListView extends StatefulWidget {
  const NameListView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NameListViewState();

}

class _NameListViewState extends State<NameListView> {

  late List<String> names;

  @override
  void initState() {
    names = ['Lorem', 'ipsum', 'dolor', 'sit', 'amet,'];
    super.initState();
  }

  void addName(String name) {
    setState(() {
      names.add(name);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, index) {
          return NameListItem(name: names[index], key: ValueKey(names[index]));
        },
        itemCount: names.length,
        padding: const EdgeInsets.symmetric(vertical: 8.0),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _displayNameInputDialog(context),
        tooltip: "Add Name",
        child: const Icon(Icons.add),
      ),
    );
  }

  Future<void> _displayNameInputDialog(BuildContext context) async {

    TextEditingController nameController = TextEditingController();

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Please Input Name"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              hintText: "Please input name here"
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                addName(nameController.value.text);
                setState(() {
                  Navigator.pop(context);
                });
                // nameController.dispose();
              },
              child: const Text("Add")
            )
          ],
        );
      },
    );
  }

}
