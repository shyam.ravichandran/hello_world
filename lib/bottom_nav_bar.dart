import 'package:flutter/material.dart';
import 'package:hello_world/bottom_nav_bar_item.dart';

class BottomNavBar extends StatefulWidget {
  final List<BottomNavBarItem> items;

  const BottomNavBar({Key? key, required this.items}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomNavBarState();

}

class _BottomNavBarState extends State<BottomNavBar> {

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.items.elementAt(_selectedIndex).title),
      ),
      body: widget.items.elementAt(_selectedIndex).content,
      bottomNavigationBar: BottomNavigationBar(
        items: widget.items.map((item) => BottomNavigationBarItem(
          label: item.label,
          icon: item.icon
        )).toList(),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
